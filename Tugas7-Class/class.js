class Animal {
    constructor(name,legs,cold_blooded) {
        this.name = name
        this.legs = 4
        this.cold_blooded = false
    }
}
var sheep = new Animal("shaun");
console.log(sheep.name) // "shaun"
console.log(sheep.legs) // 4
console.log(sheep.cold_blooded) // false
